/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.logging.Level;
import java.util.logging.Logger;
import manipulador_de_arquivo.ManipuladorArquivo;
import org.junit.Test;

/**
 *
 * @author Carlos H
 */
public class TesteLeituraDeArquivo {

    private ManipuladorArquivo manipuladorArquivo;

    public TesteLeituraDeArquivo() {
        manipuladorArquivo = new ManipuladorArquivo();
    }

    @Test
    public void testaLeituraDeArquivo() {
        try {
            this.manipuladorArquivo.leitor("B:\\UNIFAE\\SEMESTRE-8\\Integracao\\integracao_aplicacoes-trabalho1\\csv\\clientes.csv");
        } catch (Exception ex) {
            Logger.getLogger(TesteLeituraDeArquivo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
