package manipulador_de_arquivo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.charset.Charset;

/**
 *
 * @author 21452-8
 */
public class ManipuladorArquivo {

    public void leitor(String path) throws IOException, Exception {

        try (BufferedReader bufferedRead = new BufferedReader(new FileReader(path))) {
            String linha = "";

            while (true) {

                linha = bufferedRead.readLine();

                if (linha == null || linha.isEmpty()) {
                    break;
                }

                linha = new String(linha.getBytes(), Charset.forName("UTF-8"));

                buildObject(path, linha);

//                for (String string : split) {
//                    System.out.println(string);
//                }
            }

            bufferedRead.close();
        }
    }

    public void escritor(String flag, String path, Object obj) throws Exception {

        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path, true));
            bufferedWriter.append(flag + ";" + obj.toString());
            bufferedWriter.newLine();
            bufferedWriter.close();
        } catch (IOException e) {
            throw new Exception();
        }
    }

    private void buildObject(String path, String linha) throws IOException {

        ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(linha.getBytes()));
//        (Clientes) in.readObject();
        if (path.contains("cliente.csv")) {

//            for (int i = 1; i < linha.length; i++) {
//                Array.get(linha, i);
//                abstractObject.add(i);
//            }
//
        } else if (path.contains("fornecedores.csv")) {

        } else if (path.contains("produto.csv")) {

        }

    }

}
