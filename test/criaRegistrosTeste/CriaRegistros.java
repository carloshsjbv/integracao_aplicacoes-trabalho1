package criaRegistrosTeste;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import br.unifae.integracao.aplicacoes.controllers.ClientesController;
import br.unifae.integracao.aplicacoes.controllers.FornecedoresController;
import br.unifae.integracao.aplicacoes.model.Clientes;
import br.unifae.integracao.aplicacoes.model.Fornecedores;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author pr_andrade
 */
public class CriaRegistros {

    private static ClientesController cliControl = new ClientesController();
    private static FornecedoresController fornecedoresController = new FornecedoresController();
    private static Clientes cliente = null;

    public static void main(String[] args) throws ParseException {
        cadastrarClientes(10);
    }

    public static void cadastrarClientes(int quantidade) throws ParseException {
        Date date;
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        date = df.parse("18/04/1992");
        for (int i = 0; i < quantidade; i++) {
            cliente = new Clientes(0, "Paulo Andrade_" + (i + 1), date, "41529160871", "481309123", "1936085121", "19992005552", 100.00, "Sitio Santa Rita", "S/N°", "Zona Rural", "São José", "SP", null);
            cliControl.salvar(cliente);
        }
        for (int i = 0; i < quantidade; i++) {
            Fornecedores fornecedores = new Fornecedores(0, "Teste" + (i + 1), "Teste" + (i + 1), "1936337481", "19982584904", "Carlos",
                    "Teste", "1", "Ipe", "SJBV", "SP", "11111111111111", "123", null);
            fornecedoresController.salvar(fornecedores);
        }
    }
}
