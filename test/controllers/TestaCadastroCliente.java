/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import br.unifae.integracao.aplicacoes.controllers.ClientesController;
import br.unifae.integracao.aplicacoes.exceptions.DBErrorException;
import br.unifae.integracao.aplicacoes.model.Clientes;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Carlos H
 */
public class TestaCadastroCliente {

    ClientesController controller = new ClientesController();

    @Test
    public void testaInsercaoDeClienteComDataNull() {
        Clientes cliente = new Clientes(0, "Carlos", null, "44444", "44444", "44444", "44444", 0.0, "44444", "44444", "44444", "44444", "SP", null);
        assertEquals("Data errada", false, controller.salvar(cliente));
    }

    @Test
    public void testaInsercaoDeCliente() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 1994);
        calendar.set(Calendar.MONTH, 07);
        calendar.set(Calendar.DATE, 12);
        Date data = calendar.getTime();
        System.out.println(data.toInstant());

        DateFormat f = DateFormat.getDateTimeInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyy/MM/dd");
        System.out.println(f.format(data));
        System.out.println(sdf.format(data));

        Clientes cliente1 = new Clientes(0, "Carlos", data, "49203576053", "44444", "44444", "19982584904", 0.0, "44444", "44444", "44444", "44444", "SP", new Date(2018, 1, 1));
        assertEquals("Data certa", true, controller.salvar(cliente1));
    }

    @Test
    public void deleteClientes() throws DBErrorException {

        controller.delete(26);

    }

    @Test
    public void buscaPorid() {

        try {
            Clientes buscaClientesPorId = controller.buscaPorId(27);
            System.out.println(buscaClientesPorId.toString());
        } catch (ParseException ex) {
            Logger.getLogger(TestaCadastroCliente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Test
    public void buscaPorNome() {

        try {
            List<Clientes> buscaClientesPorId = controller.buscaPorNome("Carlos");

            for (Clientes clientes : buscaClientesPorId) {
                System.out.println(clientes.toString());
            }
        } catch (ParseException ex) {
            Logger.getLogger(TestaCadastroCliente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
