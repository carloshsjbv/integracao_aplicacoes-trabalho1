/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import br.unifae.integracao.aplicacoes.controllers.FornecedoresController;
import br.unifae.integracao.aplicacoes.controllers.ProdutosController;
import br.unifae.integracao.aplicacoes.exceptions.DBErrorException;
import br.unifae.integracao.aplicacoes.model.Fornecedores;
import br.unifae.integracao.aplicacoes.model.Produtos;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Carlos H
 */
public class TestaCadastroProdutos {

    ProdutosController controller = new ProdutosController();
    FornecedoresController controllerf = new FornecedoresController();

    @Test
    public void cadastraFornecedor() {
        Fornecedores buscaFornecedoresPorId = controllerf.buscaPorId(2);

        Produtos produtos = new Produtos(0,
                "Produto", "000", null, null, 0, 0, 0, 0, buscaFornecedoresPorId.getId());

        boolean salvar = controller.salvar(produtos);

    }
//
//    @Test
//    public void delete() {
//
//        try {
//            controller.delete(8);
//        } catch (DBErrorException ex) {
//            Logger.getLogger(TestaCadastroProdutos.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//    }
//
//    @Test
//    public void buscaPorId() {
//
//        Produtos buscaProdutosPorId = controller.buscaPorId(2);
//        System.out.println(buscaProdutosPorId);
//
//    }
//
//    @Test
//    public void buscaPorNome() {
//
//        List<Produtos> buscaProdutosPorNome = controller.buscaPorNome("Produto");
//
//        buscaProdutosPorNome.forEach((fornecedores) -> {
//            System.out.println(fornecedores);
//        });
//
//    }
}
