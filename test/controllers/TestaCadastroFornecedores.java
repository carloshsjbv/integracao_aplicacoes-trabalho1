/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import br.unifae.integracao.aplicacoes.controllers.FornecedoresController;
import br.unifae.integracao.aplicacoes.exceptions.DBErrorException;
import br.unifae.integracao.aplicacoes.model.Fornecedores;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Carlos H
 */
public class TestaCadastroFornecedores {

    FornecedoresController controller = new FornecedoresController();

    @Test
    public void cadastraFornecedor() {
        Fornecedores fornecedores = new Fornecedores(0, "Teste", "Teste", "1936337481", "19982584904", "Carlos",
                "Teste", "1", "Ipe", "SJBV", "SP", "11111111111111", "123", null);

        controller.salvar(fornecedores);

    }

    @Test
    public void delete() {

        try {
            controller.delete(1);
        } catch (DBErrorException ex) {
            Logger.getLogger(TestaCadastroFornecedores.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Test
    public void buscaPorId() {

        Fornecedores buscaFornecedoresPorId = controller.buscaPorId(2);
        System.out.println(buscaFornecedoresPorId);

    }

    @Test
    public void buscaPorNome() {

        List<Fornecedores> buscaFornecedoresPorNome = controller.buscaPorNome("Teste");

        buscaFornecedoresPorNome.forEach((fornecedores) -> {
            System.out.println(fornecedores);
        });

    }

}
