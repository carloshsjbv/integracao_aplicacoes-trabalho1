package br.unifae.integracao.aplicacoes.controllers;

import br.unifae.integracao.aplicacoes.dao.ProdutosDAO;
import br.unifae.integracao.aplicacoes.exceptions.DBErrorException;
import br.unifae.integracao.aplicacoes.model.Produtos;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlos Francisco
 */
public class ProdutosController {

    ProdutosDAO produtosDAO = new ProdutosDAO();

//    private Map<String, String> validaCampos(Produtos produtos) {
//        Map<String, String> validations = new HashMap<>();
//        // Validations
//        if (produtos.getNome() == null || produtos.getNome().isEmpty()) {
//            validations.put("nome", "Campo obrigatório");
//        }
//        if (produtos.getVlr_custo() < 0) {
//            validations.put("vlr_custo", "Campo obrigatório");
//        }
//        if (produtos.getVlr_venda() < 0) {
//            validations.put("vlr_venda", "Campo obrigatório");
//        }
//        if (produtos.getEstoque() < 0) {
//            validations.put("estoque", "Campo obrigatório");
//        }
//        if (produtos.getFornecedores_id() < 0) {
//            validations.put("fornecedor", "Campo obrigatório");
//        }
//        return validations;
//    }
    public boolean salvar(Produtos produtos) {

//        Map<String, String> validations = validaCampos(produtos);
//        if (validations == null || validations.isEmpty()) {
        try {
            produtosDAO.salvar(produtos);
            return true;
        } catch (DBErrorException ex) {
            Logger.getLogger(ProdutosController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

//        } else {
//            return false;
//        }
    }

    public boolean update(Produtos produtos) {

//        Map<String, String> validations = validaCampos(produtos);
//        if (validations == null || validations.isEmpty()) {
        try {
            produtosDAO.salvar(produtos);
            return true;
        } catch (DBErrorException ex) {
            Logger.getLogger(ProdutosController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

//        } else {
//            return false;
//        }
    }

    public void delete(int id) throws DBErrorException {

        produtosDAO.delete(id);

    }

    public Produtos buscaPorId(int id) {

        Produtos produtos = null;

        try {
            produtos = produtosDAO.buscaProdutosPorId(id);
        } catch (DBErrorException ex) {
            Logger.getLogger(ProdutosController.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (produtos != null) {
            return produtos;
        } else {
            return null;
        }

    }

    public List<Produtos> buscaPorNome(String nome) {

        List<Produtos> produtos = null;

        try {
            produtos = produtosDAO.buscaProdutosPorNome(nome);
        } catch (DBErrorException ex) {
            Logger.getLogger(ProdutosController.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (produtos != null) {
            return produtos;
        } else {
            return null;
        }
    }

    public int buscaUltimoIdInserido() {
        try {
            return produtosDAO.buscaUltimoIdInserido();
        } catch (DBErrorException ex) {
            Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public List<Produtos> listaProdutos() {

        List<Produtos> produtos = null;

        try {
            produtos = produtosDAO.listraProdutos();
        } catch (DBErrorException ex) {
            Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (produtos != null) {
            return produtos;
        } else {
            return null;
        }
    }
}
