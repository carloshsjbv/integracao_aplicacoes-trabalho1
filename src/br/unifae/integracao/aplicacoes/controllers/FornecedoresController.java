package br.unifae.integracao.aplicacoes.controllers;

import br.unifae.integracao.aplicacoes.dao.FornecedoresDAO;
import br.unifae.integracao.aplicacoes.exceptions.DBErrorException;
import br.unifae.integracao.aplicacoes.model.Fornecedores;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlos Francisco
 */
public class FornecedoresController {

    FornecedoresDAO fornecedoresDAO = new FornecedoresDAO();

//    private Map<String, String> validaCampos(Fornecedores fornecedores) {
//        Map<String, String> validations = new HashMap<>();
//        // Validations
//        if (fornecedores.getRazao_social() == null || fornecedores.getRazao_social().isEmpty()) {
//            validations.put("razao_social", "Campo obrigatório");
//        }
//        if (fornecedores.getTelefone() == null || fornecedores.getRazao_social().isEmpty()) {
//            validations.put("telefone", "Campo obrigatório");
//        } else if (fornecedores.getTelefone().length() > 10 || !fornecedores.getTelefone().matches("[1-9]{2}[1-9][0-9]{7}")) {
//            validations.put("celular", "Valor inválido");
//        }
//        if (fornecedores.getCelular() == null || fornecedores.getCelular().isEmpty()) {
//            validations.put("celular", "Campo obrigatório");
//        } else if (fornecedores.getCelular().length() > 11 || !fornecedores.getCelular().matches("[1-9]{2}[1-9][0-9]{8}")) {
//            validations.put("celular", "Valor inválido");
//        }
//        if (fornecedores.getNome_contato() == null || fornecedores.getNome_contato().isEmpty()) {
//            validations.put("nome_contato", "Campo obrigatório");
//        }
//        if (fornecedores.getCnpj() == null || fornecedores.getCnpj().isEmpty()) {
//            validations.put("cnpj", "Campo obrigatório");
//        } else if (fornecedores.getCnpj().length() != 14) {
//            validations.put("cnpj", "Valor inválido");
//        }
//        if (fornecedores.getInscricao() == null || fornecedores.getInscricao().isEmpty()) {
//            validations.put("inscricao", "Campo obrigatório");
//        }
//        return validations;
//    }
    public boolean salvar(Fornecedores fornecedores) {

//        Map<String, String> validations = validaCampos(fornecedores);
//
//        if (validations == null || validations.isEmpty()) {
        try {
            fornecedoresDAO.salvar(fornecedores);
            return true;
        } catch (DBErrorException ex) {
            Logger.getLogger(FornecedoresController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    public boolean update(Fornecedores fornecedores) {

//        Map<String, String> validations = validaCampos(fornecedores);
//        if (validations == null || validations.isEmpty()) {
        try {
            fornecedoresDAO.salvar(fornecedores);
            return true;
        } catch (DBErrorException ex) {
            Logger.getLogger(FornecedoresController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    public void delete(int id) throws DBErrorException {

        fornecedoresDAO.delete(id);

    }

    public Fornecedores buscaPorId(int id) {

        Fornecedores fornecedores = null;

        try {
            fornecedores = fornecedoresDAO.buscaFornecedoresPorId(id);
        } catch (DBErrorException ex) {
            Logger.getLogger(FornecedoresController.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (fornecedores != null) {
            return fornecedores;
        } else {
            return null;
        }

    }

    public List<Fornecedores> buscaPorNome(String nome) {

        List<Fornecedores> fornecedores = null;

        try {
            fornecedores = fornecedoresDAO.buscaFornecedoresPorNome(nome);
        } catch (DBErrorException ex) {
            Logger.getLogger(FornecedoresController.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (fornecedores != null) {
            return fornecedores;
        } else {
            return null;
        }
    }

    public int buscaUltimoIdInserido() {
        try {
            return fornecedoresDAO.buscaUltimoIdInserido();
        } catch (DBErrorException ex) {
            Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public List<Fornecedores> listaFornecedores() {

        List<Fornecedores> fornecedores = null;

        try {
            fornecedores = fornecedoresDAO.listaFornecedores();
        } catch (DBErrorException ex) {
            Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (fornecedores != null) {
            return fornecedores;
        } else {
            return null;
        }
    }
}
