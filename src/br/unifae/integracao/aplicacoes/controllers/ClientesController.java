package br.unifae.integracao.aplicacoes.controllers;

import br.unifae.integracao.aplicacoes.dao.ClientesDAO;
import br.unifae.integracao.aplicacoes.exceptions.DBErrorException;
import br.unifae.integracao.aplicacoes.model.Clientes;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlos Francisco
 */
public class ClientesController {

    ClientesDAO clientesDAO = new ClientesDAO();

//    private Map<String, String> validaCampos(Clientes cliente) {
//        Map<String, String> validations = new HashMap<>();
//        // Validations
//
//        Calendar calendar = Calendar.getInstance();
//        calendar.set(Calendar.YEAR, 1900);
//        calendar.set(Calendar.MONTH, 01);
//        calendar.set(Calendar.DATE, 11);
//        Date dataMinima = calendar.getTime();
//        System.out.println(dataMinima.toInstant());
//
//        if (cliente.getNome() == null || cliente.getNome().isEmpty()) {
//            validations.put("nome", "Campo obrigatório");
//        }
//        if (cliente.getDat_nascimento() == null) {
//            validations.put("dat_nascimento", "Campo obrigatório");
//        } else if (cliente.getDat_nascimento().before(dataMinima)) {
//            validations.put("dat_nascimento", "Valor inválido");
//        }
//        if (cliente.getCpf() == null || cliente.getCpf().isEmpty()) {
//            validations.put("cpf", "Campo obrigatório");
//        } else if (cliente.getCpf().length() != 11) {
//            validations.put("cpf", "Valor inválido");
//        }
//        if (cliente.getRg() == null || cliente.getRg().isEmpty()) {
//            validations.put("rg", "Campo obrigatório");
//        }
//        if (cliente.getCelular() == null || cliente.getCelular().isEmpty()) {
//            validations.put("celular", "Campo obrigatório");
//        } else if (cliente.getCelular().length() > 11 || !cliente.getCelular().matches("[1-9]{2}[1-9][0-9]{8}")) {
//            validations.put("celular", "Valor inválido");
//        }
//        if (cliente.getLimite_credito() < 0) {
//            validations.put("limite_credito", "Limite negativo");
//        }
//        return validations;
//    }
    public boolean salvar(Clientes cliente) {

//        Map<String, String> validations = validaCampos(cliente);
//
//        if (validations == null || validations.isEmpty()) {
        try {
            clientesDAO.salvar(cliente);
            return true;

        } catch (DBErrorException ex) {
            Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    public boolean update(Clientes cliente) {

//        Map<String, String> validations = validaCampos(cliente);
//        if (validations == null || validations.isEmpty()) {
        try {
            clientesDAO.salvar(cliente);
            return true;
        } catch (DBErrorException ex) {
            Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    public void delete(int id) throws DBErrorException {
        clientesDAO.delete(id);
    }

    public Clientes buscaPorId(int id) throws ParseException {

        Clientes cliente = null;

        try {
            cliente = clientesDAO.buscaClientesPorId(id);
        } catch (DBErrorException ex) {
            Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (cliente != null) {
            return cliente;
        } else {
            return null;
        }
    }

    public List<Clientes> buscaPorNome(String nome) throws ParseException {

        List<Clientes> clientes = null;

        try {
            clientes = clientesDAO.buscaClientessPorNome(nome);
        } catch (DBErrorException ex) {
            Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (clientes != null) {
            return clientes;
        } else {
            return null;
        }
    }

    public int buscaUltimoIdInserido() {
        try {
            return clientesDAO.buscaUltimoIdInserido();
        } catch (DBErrorException ex) {
            Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public List<Clientes> listaClientes() throws ParseException {

        List<Clientes> clientes = null;

        try {
            clientes = clientesDAO.listaClientes();
        } catch (DBErrorException ex) {
            Logger.getLogger(ClientesController.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (clientes != null) {
            return clientes;
        } else {
            return null;
        }
    }
}
