package br.unifae.integracao.aplicacoes.dao;

import br.unifae.integracao.aplicacoes.exceptions.DBErrorException;
import br.unifae.integracao.aplicacoes.integracao.ManipuladorArquivo;
import br.unifae.integracao.aplicacoes.model.Fornecedores;
import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlos Francisco
 */
public class FornecedoresDAO {

    public void salvar(Fornecedores fornecedor) throws DBErrorException {

        if (fornecedor.getId() == 0) {
            inserir(fornecedor);
        } else {
            update(fornecedor);
        }
    }

    private void inserir(Fornecedores fornecedor) throws DBErrorException {
        PreparedStatement ps = null;

        try {
            ps = DBConnection.getConexao().prepareStatement(""
                    + "INSERT INTO FORNECEDORES"
                    + "("
                    + "razao_social ,"
                    + "nome_fantasia ,"
                    + "telefone ,"
                    + "celular ,"
                    + "nome_contato ,"
                    + "endereco ,"
                    + "numero ,"
                    + "bairro ,"
                    + "cidade ,"
                    + "estado ,"
                    + "cnpj ,"
                    + "inscricao "
                    + ") "
                    + "VALUES("
                    + " ?,"
                    + " ?,"
                    + " ?,"
                    + " ?,"
                    + " ?,"
                    + " ?,"
                    + " ?,"
                    + " ?,"
                    + " ?,"
                    + " ?,"
                    + " ?,"
                    + " ?"
                    + ")");

            ps.setString(1, fornecedor.getRazao_social());
            ps.setString(2, fornecedor.getNome_fantasia());
            ps.setString(3, fornecedor.getTelefone());
            ps.setString(4, fornecedor.getCelular());
            ps.setString(5, fornecedor.getNome_contato());
            ps.setString(6, fornecedor.getEndereco());
            ps.setString(7, fornecedor.getNumero());
            ps.setString(8, fornecedor.getBairro());
            ps.setString(9, fornecedor.getCidade());
            ps.setString(10, String.valueOf(fornecedor.getEstado()));
            ps.setString(11, fornecedor.getCnpj());
            ps.setString(12, fornecedor.getInscricao());

            if (ps.executeUpdate() == 0) {
                throw new DBErrorException("Ocorreu um erro durante o cadastro do Fornecedores. Entre em contato com o administrador.");
            } else {
                fornecedor.setId(buscaUltimoIdInserido());
                DBConnection.getConexao().commit();
                try {
                    integracao(fornecedor, "0");
                } catch (IOException ex) {
                    Logger.getLogger(FornecedoresDAO.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
    }

    private void update(Fornecedores fornecedor) throws DBErrorException {
        PreparedStatement ps = null;

        try {
            ps = DBConnection.getConexao().prepareStatement("UPDATE "
                    + "FORNECEDORES SET "
                    + "razao_social = ? ,"
                    + "nome_fantasia = ? ,"
                    + "telefone = ? ,"
                    + "celular = ? ,"
                    + "nome_contato = ? ,"
                    + "endereco = ? ,"
                    + "numero = ? ,"
                    + "bairro = ? ,"
                    + "cidade = ? ,"
                    + "estado = ? ,"
                    + "cnpj = ? ,"
                    + "inscricao = ? ,"
                    + "dat_ultima_compra = ? "
                    + " WHERE id = ?"
            );
            ps.setString(1, fornecedor.getRazao_social());
            ps.setString(2, fornecedor.getNome_fantasia());
            ps.setString(3, fornecedor.getTelefone());
            ps.setString(4, fornecedor.getCelular());
            ps.setString(5, fornecedor.getNome_contato());
            ps.setString(6, fornecedor.getEndereco());
            ps.setString(7, fornecedor.getNumero());
            ps.setString(8, fornecedor.getBairro());
            ps.setString(9, fornecedor.getCidade());
            ps.setString(10, String.valueOf(fornecedor.getEstado()));
            ps.setString(11, fornecedor.getCnpj());
            ps.setString(12, fornecedor.getInscricao());
            ps.setDate(13, (Date) fornecedor.getDat_ultima_compra());
            ps.setInt(14, fornecedor.getId());

            if (ps.executeUpdate() == 0) {
                throw new DBErrorException("Ocorreu um erro durante a atualização dos dados do Fornecedores. Entre em contato com o administrador.");
            } else {
                DBConnection.getConexao().commit();
                try {
                    integracao(fornecedor, "1");
                } catch (IOException ex) {
                    Logger.getLogger(FornecedoresDAO.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
    }

    public void delete(int id) throws DBErrorException {
        PreparedStatement ps = null;

        try {
            //antes de deletar reserva o fornecedor para passar para o manipulador de arquivos
            Fornecedores fornecedorExcluido = buscaFornecedoresPorId(id);

            String query = "DELETE FROM FORNECEDORES WHERE id = ?";
            ps = DBConnection.getConexao().prepareStatement(query);

            ps.setInt(1, id);

            if (ps.executeUpdate() == 0) {
                throw new DBErrorException("Ocorreu um erro durante a exclusão do Fornecedores. Entre em contato com o administrador.");
            } else {
                DBConnection.getConexao().commit();
                try {
                    integracao(fornecedorExcluido, "2");
                } catch (IOException ex) {
                    Logger.getLogger(FornecedoresDAO.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
    }

    public int buscaUltimoIdInserido() throws DBErrorException {

        Statement st = null;
        ResultSet rs = null;

        try {
            st = DBConnection.getConexao().createStatement();
            rs = st.executeQuery("SELECT LAST_INSERT_ID() AS id");

            while (rs.next()) {
                return rs.getInt("id");
            }
        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                rs.close();
                st.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
        return 0;
    }

    public Fornecedores buscaFornecedoresPorId(int id) throws DBErrorException {

        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            String query = "SELECT * FROM FORNECEDORES WHERE id = " + id;
            ps = DBConnection.getConexao().prepareStatement(query);

            rs = ps.executeQuery(query);

            while (rs.next()) {
                return new Fornecedores(
                        rs.getInt("id"),
                        rs.getString("razao_social"),
                        rs.getString("nome_fantasia"),
                        rs.getString("telefone"),
                        rs.getString("celular"),
                        rs.getString("nome_contato"),
                        rs.getString("endereco"),
                        rs.getString("numero"),
                        rs.getString("bairro"),
                        rs.getString("cidade"),
                        rs.getString("estado"),
                        rs.getString("cnpj"),
                        rs.getString("inscricao"),
                        rs.getTimestamp("dat_ultima_compra"));
            }

        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }

        return null;
    }

    public ArrayList<Fornecedores> buscaFornecedoresPorNome(String nome) throws DBErrorException {

        PreparedStatement ps = null;
        ResultSet rs = null;

        ArrayList<Fornecedores> lista = new ArrayList<>();

        try {
            String query = "SELECT * FROM Fornecedores WHERE nome_fantasia like ?";
            ps = DBConnection.getConexao().prepareStatement(query);

            ps.setString(1, "%" + nome + "%");

            rs = ps.executeQuery();

            while (rs.next()) {
                lista.add(new Fornecedores(
                        rs.getInt("id"),
                        rs.getString("razao_social"),
                        rs.getString("nome_fantasia"),
                        rs.getString("telefone"),
                        rs.getString("celular"),
                        rs.getString("nome_contato"),
                        rs.getString("endereco"),
                        rs.getString("numero"),
                        rs.getString("bairro"),
                        rs.getString("cidade"),
                        rs.getString("estado"),
                        rs.getString("cnpj"),
                        rs.getString("inscricao"),
                        rs.getTimestamp("dat_ultima_compra")));
            }
        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
        return lista;
    }

    public List<Fornecedores> listaFornecedores() throws DBErrorException {

        PreparedStatement ps = null;
        ResultSet rs = null;

        List<Fornecedores> lista = new ArrayList<>();

        try {
            String query = "SELECT * FROM Fornecedores";
            ps = DBConnection.getConexao().prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                lista.add(new Fornecedores(
                        rs.getInt("id"),
                        rs.getString("razao_social"),
                        rs.getString("nome_fantasia"),
                        rs.getString("telefone"),
                        rs.getString("celular"),
                        rs.getString("nome_contato"),
                        rs.getString("endereco"),
                        rs.getString("numero"),
                        rs.getString("bairro"),
                        rs.getString("cidade"),
                        rs.getString("estado"),
                        rs.getString("cnpj"),
                        rs.getString("inscricao"),
                        rs.getTimestamp("dat_ultima_compra")));
            }
        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
        return lista;
    }

    private void integracao(Fornecedores fornecedor, String flag) throws IOException, Exception {
        ManipuladorArquivo ma = new ManipuladorArquivo();
        String filePath = "csv/fornecedores.csv";
        File arquivo = new File(filePath);

        if (!arquivo.exists()) {
            arquivo.createNewFile();
        }
        ma.escritor(flag, filePath, fornecedor);
    }
}
