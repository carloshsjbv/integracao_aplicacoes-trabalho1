package br.unifae.integracao.aplicacoes.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Carlos Franscico
 */
public class DBConnection {

    public static Connection con = null;

    public static Connection getConexao() throws SQLException, ClassNotFoundException {
        if (con == null) {
            criaConexao();
        }

        return con;
    }

    public static void criaConexao() throws SQLException, ClassNotFoundException {
//        DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());

        Class.forName("com.mysql.cj.jdbc.Driver");
        con = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/trabalho1?useTimezone=true&serverTimezone=UTC&useSSL=false",
                "root",
                "root"
        );

        con.setAutoCommit(false);
        con.setTransactionIsolation(con.TRANSACTION_READ_COMMITTED);

    }

}
