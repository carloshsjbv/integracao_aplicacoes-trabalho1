package br.unifae.integracao.aplicacoes.dao;

import br.unifae.integracao.aplicacoes.exceptions.DBErrorException;
import br.unifae.integracao.aplicacoes.integracao.ManipuladorArquivo;
import br.unifae.integracao.aplicacoes.model.Produtos;
import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlos Francisco
 */
public class ProdutosDAO {

    public void salvar(Produtos produto) throws DBErrorException {

        if (produto.getId() == 0) {
            inserir(produto);
        } else {
            update(produto);
        }
    }

    private void inserir(Produtos produto) throws DBErrorException {
        PreparedStatement ps = null;

        try {
            ps = DBConnection.getConexao().prepareStatement(""
                    + "INSERT INTO PRODUTOS"
                    + "("
                    + "nome, "
                    + "codigo_barras, "
                    + "vlr_custo, "
                    + "vlr_venda, "
                    + "margem_lucro, "
                    + "estoque, "
                    + "fornecedores_id"
                    + ") "
                    + "VALUES("
                    + "?, "
                    + "?, "
                    + "?, "
                    + "?, "
                    + "?, "
                    + "?, "
                    + "?"
                    + ")");

            ps.setString(1, produto.getNome());
            ps.setString(2, produto.getCodigoBarras());
            ps.setDouble(3, produto.getVlr_custo());
            ps.setDouble(4, produto.getVlr_venda());
            ps.setDouble(5, produto.getMargem_lucro());
            ps.setInt(6, produto.getEstoque());
            ps.setInt(7, produto.getFornecedores_id());

            if (ps.execute()) {
                throw new DBErrorException("Ocorreu um erro durante o cadastro do Produtos. Entre em contato com o administrador.");
            } else {
                produto.setId(buscaUltimoIdInserido());
                DBConnection.getConexao().commit();
                try {
                    integracao(produto, "0");
                } catch (IOException ex) {
                    Logger.getLogger(ProdutosDAO.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
    }

    private void update(Produtos produto) throws DBErrorException {
        PreparedStatement ps = null;

        try {
            ps = DBConnection.getConexao().prepareStatement("UPDATE "
                    + "PRODUTOS SET "
                    + "nome = ?, "
                    + "codigo_barras = ?, "
                    + "dat_ultima_compra = ?, "
                    + "dat_ultima_venda = ?, "
                    + "vlr_custo = ?, "
                    + "vlr_venda = ?, "
                    + "margem_lucro = ?, "
                    + "estoque = ?, "
                    + "fornecedores_id = ?"
                    + " WHERE id = ?"
            );
            ps.setString(1, produto.getNome());
            ps.setString(2, produto.getCodigoBarras());
            ps.setDate(3, (Date) produto.getDat_ultima_compra());
            ps.setDate(4, (Date) produto.getDat_ultima_venda());
            ps.setDouble(5, produto.getVlr_custo());
            ps.setDouble(6, produto.getVlr_venda());
            ps.setDouble(7, produto.getMargem_lucro());
            ps.setInt(8, produto.getEstoque());
            ps.setObject(9, produto.getFornecedores_id());
            ps.setInt(10, produto.getId());

            if (ps.executeUpdate() == 0) {
                throw new DBErrorException("Ocorreu um erro durante a atualização dos dados do Produtos. Entre em contato com o administrador.");
            } else {
                DBConnection.getConexao().commit();
                try {
                    integracao(produto, "1");
                } catch (IOException ex) {
                    Logger.getLogger(ProdutosDAO.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
    }

    public void delete(int id) throws DBErrorException {
        PreparedStatement ps = null;

        try {
            //antes de deletar reserva o produto para passar para o manipulador de arquivos
            Produtos produtoExcluido = buscaProdutosPorId(id);
            
            String query = "DELETE FROM PRODUTOS WHERE id = " + id;
            ps = DBConnection.getConexao().prepareStatement(query);

            if (ps.executeUpdate() == 0) {
                throw new DBErrorException("Ocorreu um erro durante a exclusão do Produtos. Entre em contato com o administrador.");
            } else {
                DBConnection.getConexao().commit();
                try {
                    integracao(produtoExcluido, "2");
                } catch (IOException ex) {
                    Logger.getLogger(ProdutosDAO.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
    }

    public int buscaUltimoIdInserido() throws DBErrorException {

        Statement st = null;
        ResultSet rs = null;

        try {
            st = DBConnection.getConexao().createStatement();
            rs = st.executeQuery("SELECT LAST_INSERT_ID() AS id");

            while (rs.next()) {
                return rs.getInt("id");
            }
        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                rs.close();
                st.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
        return 0;
    }

    public Produtos buscaProdutosPorId(int id) throws DBErrorException {

        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            String query = "SELECT * FROM PRODUTOS WHERE id = " + id;
            ps = DBConnection.getConexao().prepareStatement(query);

            rs = ps.executeQuery(query);

            while (rs.next()) {
                return new Produtos(
                        rs.getInt("id"),
                        rs.getString("nome"),
                        rs.getString("codigo_barras"),
                        rs.getTimestamp("dat_ultima_compra"),
                        rs.getTimestamp("dat_ultima_venda"),
                        rs.getDouble("vlr_custo"),
                        rs.getDouble("vlr_venda"),
                        rs.getDouble("margem_lucro"),
                        rs.getInt("estoque"),
                        rs.getInt("fornecedores_id"));
            }

        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }

        return null;
    }

    public ArrayList<Produtos> buscaProdutosPorNome(String nome) throws DBErrorException {

        PreparedStatement ps = null;
        ResultSet rs = null;

        ArrayList<Produtos> lista = new ArrayList<>();

        try {
            String query = "SELECT * FROM Produtos WHERE nome like ?";
            ps = DBConnection.getConexao().prepareStatement(query);

            ps.setString(1, "%" + nome + "%");

            rs = ps.executeQuery();

            while (rs.next()) {
                lista.add(new Produtos(
                        rs.getInt("id"),
                        rs.getString("nome"),
                        rs.getString("codigo_barras"),
                        rs.getTimestamp("dat_ultima_compra"),
                        rs.getTimestamp("dat_ultima_venda"),
                        rs.getDouble("vlr_custo"),
                        rs.getDouble("vlr_venda"),
                        rs.getDouble("margem_lucro"),
                        rs.getInt("estoque"),
                        rs.getInt("fornecedores_id")));
            }

        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
        return lista;
    }

    public List<Produtos> listraProdutos() throws DBErrorException {

        PreparedStatement ps = null;
        ResultSet rs = null;

        List<Produtos> lista = new ArrayList<>();

        try {
            String query = "SELECT * FROM Produtos";
            ps = DBConnection.getConexao().prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                lista.add(new Produtos(
                        rs.getInt("id"),
                        rs.getString("nome"),
                        rs.getString("codigo_barras"),
                        rs.getTimestamp("dat_ultima_compra"),
                        rs.getTimestamp("dat_ultima_venda"),
                        rs.getDouble("vlr_custo"),
                        rs.getDouble("vlr_venda"),
                        rs.getDouble("margem_lucro"),
                        rs.getInt("estoque"),
                        rs.getInt("fornecedores_id")));
            }
        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
        return lista;
    }
    
    private void integracao(Produtos produto, String flag) throws IOException, Exception {
        ManipuladorArquivo ma = new ManipuladorArquivo();
        String filePath = "csv/produtos.csv";
        File arquivo = new File(filePath);

        if (!arquivo.exists()) {
            arquivo.createNewFile();
        }
        ma.escritor(flag, filePath, produto);
    }
}
