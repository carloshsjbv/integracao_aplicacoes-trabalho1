package br.unifae.integracao.aplicacoes.dao;

import br.unifae.integracao.aplicacoes.exceptions.DBErrorException;
import br.unifae.integracao.aplicacoes.integracao.ManipuladorArquivo;
import br.unifae.integracao.aplicacoes.model.Clientes;
import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carlos Francisco
 */
public class ClientesDAO {

    public void salvar(Clientes cliente) throws DBErrorException {

        if (cliente.getId() == 0) {
            inserir(cliente);
        } else {
            update(cliente);
        }
    }

    private void inserir(Clientes cliente) throws DBErrorException {
        PreparedStatement ps = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

        try {
            String query = "INSERT INTO CLIENTES"
                    + "(nome, dat_nascimento, cpf, rg , telefone , celular , limite_credito , "
                    + "endereco , numero , bairro , cidade , estado)"
                    + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            ps = DBConnection.getConexao().prepareStatement(query);

            ps.setString(1, cliente.getNome());
            ps.setString(2, sdf.format(cliente.getDat_nascimento()));
            ps.setString(3, cliente.getCpf());
            ps.setString(4, cliente.getRg());
            ps.setString(5, cliente.getTelefone());
            ps.setString(6, cliente.getCelular());
            ps.setDouble(7, cliente.getLimite_credito());
            ps.setString(8, cliente.getEndereco());
            ps.setString(9, cliente.getNumero());
            ps.setString(10, cliente.getBairro());
            ps.setString(11, cliente.getCidade());
            ps.setString(12, String.valueOf(cliente.getEstado()));

            if (ps.executeUpdate() == 0) {
                throw new DBErrorException("Ocorreu um erro durante o cadastro do Clientes. Entre em contato com o administrador.");
            } else {
                cliente.setId(buscaUltimoIdInserido());
                DBConnection.getConexao().commit();
                try {
                    integracao(cliente, "0");
                } catch (IOException ex) {
                    Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
    }

    private void update(Clientes cliente) throws DBErrorException {
        PreparedStatement ps = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

        try {
            ps = DBConnection.getConexao().prepareStatement("UPDATE "
                    + "CLIENTES SET "
                    + "nome = ? ,"
                    + "dat_nascimento = ? ,"
                    + "cpf = ? ,"
                    + "rg = ? ,"
                    + "telefone = ? ,"
                    + "celular = ? ,"
                    + "limite_credito = ? ,"
                    + "endereco = ? ,"
                    + "numero = ? ,"
                    + "bairro = ? ,"
                    + "cidade = ? ,"
                    + "estado = ? "
                    + " WHERE id = ?"
            );
            ps.setString(1, cliente.getNome());
            ps.setString(2, sdf.format(cliente.getDat_nascimento().getTime()));
            ps.setString(3, cliente.getCpf());
            ps.setString(4, cliente.getRg());
            ps.setString(5, cliente.getTelefone());
            ps.setString(6, cliente.getCelular());
            ps.setDouble(7, cliente.getLimite_credito());
            ps.setString(8, cliente.getEndereco());
            ps.setString(9, cliente.getNumero());
            ps.setString(10, cliente.getBairro());
            ps.setString(11, cliente.getCidade());
            ps.setString(12, String.valueOf(cliente.getEstado()));
            ps.setInt(13, cliente.getId());

            if (ps.executeUpdate() == 0) {
                throw new DBErrorException("Ocorreu um erro durante a atualização dos dados do Clientes. Entre em contato com o administrador.");
            } else {
                DBConnection.getConexao().commit();
                try {
                    integracao(cliente, "1");
                } catch (IOException ex) {
                    Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
    }

    public void delete(int id) throws DBErrorException {
        PreparedStatement ps = null;

        try {
            //antes de deletar reserva o cliente para passar para o manipulador de arquivos
            Clientes clienteExcluido = buscaClientesPorId(id);

            String query = "DELETE FROM CLIENTES WHERE id = ?";
            ps = DBConnection.getConexao().prepareStatement(query);

            ps.setInt(1, id);

            if (ps.executeUpdate() == 0) {
                throw new DBErrorException("Ocorreu um erro durante a exclusão do Clientes. Entre em contato com o administrador.");
            } else {
                DBConnection.getConexao().commit();
                try {
                    integracao(clienteExcluido, "2");
                } catch (IOException ex) {
                    Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } catch (ClassNotFoundException | SQLException | ParseException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
    }

    public int buscaUltimoIdInserido() throws DBErrorException {

        Statement st = null;
        ResultSet rs = null;

        try {
            st = DBConnection.getConexao().createStatement();
            rs = st.executeQuery("SELECT LAST_INSERT_ID() AS id");

            while (rs.next()) {
                return rs.getInt("id");
            }
        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                rs.close();
                st.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }
        return 0;
    }

    public Clientes buscaClientesPorId(int id) throws DBErrorException, ParseException {

        PreparedStatement ps = null;
        ResultSet rs = null;

        SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");

        try {
            String query = "SELECT * FROM CLIENTES WHERE id = ?";
            ps = DBConnection.getConexao().prepareStatement(query);

            ps.setInt(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                return new Clientes(
                        rs.getInt("id"),
                        rs.getString("nome"),
                        formato.parse(rs.getString("dat_nascimento").replace("-", "/")),
                        rs.getString("cpf"),
                        rs.getString("rg"),
                        rs.getString("telefone"),
                        rs.getString("celular"),
                        rs.getDouble("limite_credito"),
                        rs.getString("endereco"),
                        rs.getString("numero"),
                        rs.getString("bairro"),
                        rs.getString("cidade"),
                        rs.getString("estado"),
                        rs.getTimestamp("dat_ultima_venda"));
            }

        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }

        return null;
    }

    public ArrayList<Clientes> buscaClientessPorNome(String nome) throws DBErrorException, ParseException {

        PreparedStatement ps = null;
        ResultSet rs = null;

        SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");

        ArrayList<Clientes> lista = new ArrayList<>();

        try {
            String query = "SELECT * FROM Clientes WHERE nome like ?";
            ps = DBConnection.getConexao().prepareStatement(query);

            ps.setString(1, "%" + nome + "%");

            rs = ps.executeQuery();

            while (rs.next()) {
                lista.add(new Clientes(
                        rs.getInt("id"),
                        rs.getString("nome"),
                        formato.parse(rs.getString("dat_nascimento").replace("-", "/")),
                        rs.getString("cpf"),
                        rs.getString("rg"),
                        rs.getString("telefone"),
                        rs.getString("celular"),
                        rs.getDouble("limite_credito"),
                        rs.getString("endereco"),
                        rs.getString("numero"),
                        rs.getString("bairro"),
                        rs.getString("cidade"),
                        rs.getString("estado"),
                        rs.getTimestamp("dat_ultima_venda")));
            }

        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }

        return lista;
    }

    public List<Clientes> listaClientes() throws DBErrorException, ParseException {

        Statement st = null;
        ResultSet rs = null;

        List<Clientes> lista = new ArrayList<>();

        try {
            st = DBConnection.getConexao().createStatement();
            rs = st.executeQuery("SELECT * FROM Clientes");

            SimpleDateFormat formato = new SimpleDateFormat("yyyy/MM/dd");

            while (rs.next()) {
                lista.add(new Clientes(
                        rs.getInt("id"),
                        rs.getString("nome"),
                        formato.parse(rs.getString("dat_nascimento").replace("-", "/")),
                        rs.getString("cpf"),
                        rs.getString("rg"),
                        rs.getString("telefone"),
                        rs.getString("celular"),
                        rs.getDouble("limite_credito"),
                        rs.getString("endereco"),
                        rs.getString("numero"),
                        rs.getString("bairro"),
                        rs.getString("cidade"),
                        rs.getString("estado"),
                        rs.getTimestamp("dat_ultima_venda")));
            }

        } catch (ClassNotFoundException | SQLException ex) {
            throw new DBErrorException(ex.getMessage());
        } finally {
            try {
                rs.close();
                st.close();
            } catch (SQLException ex) {
                throw new DBErrorException(ex.getMessage());
            }
        }

        return lista;
    }

    private void integracao(Clientes cliente, String flag) throws IOException, Exception {
        ManipuladorArquivo ma = new ManipuladorArquivo();
        String filePath = "csv/clientes.csv";
        File arquivo = new File(filePath);

        if (!arquivo.exists()) {
            arquivo.createNewFile();
        }
        ma.escritor(flag, filePath, cliente);
    }
}
