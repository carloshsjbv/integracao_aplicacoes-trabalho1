package br.unifae.integracao.aplicacoes.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Carlos Francisco
 */
public class Fornecedores implements Serializable{

    private static final long serialVersionUID = 1L;

    private int id;
    private String razao_social;
    private String nome_fantasia;
    private String telefone;
    private String celular;
    private String nome_contato;
    private String endereco;
    private String numero;
    private String bairro;
    private String cidade;
    private String estado;
    private String cnpj;
    private String inscricao;
    private Date dat_ultima_compra;

    public Fornecedores() {
    }

    public Fornecedores(int id) {
        this.id = id;
    }

    public Fornecedores(
            int id,
            String razaoSocial,
            String nomeFantasia,
            String telefone,
            String celular,
            String nomeContato,
            String endereco,
            String numero,
            String bairro,
            String cidade,
            String estado,
            String cnpj,
            String inscricao,
            Date detaUltimaCompra
    ) {
        this.id = id;
        this.razao_social = razaoSocial;
        this.nome_fantasia = nomeFantasia;
        this.telefone = telefone;
        this.celular = celular;
        this.nome_contato = nomeContato;
        this.endereco = endereco;
        this.numero = numero;
        this.bairro = bairro;
        this.cidade = cidade;
        this.estado = estado;
        this.cnpj = cnpj;
        this.inscricao = inscricao;
        this.dat_ultima_compra = detaUltimaCompra;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRazao_social() {
        return razao_social;
    }

    public void setRazao_social(String razao_social) {
        this.razao_social = razao_social;
    }

    public String getNome_fantasia() {
        return nome_fantasia;
    }

    public void setNome_fantasia(String nome_fantasia) {
        this.nome_fantasia = nome_fantasia;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getNome_contato() {
        return nome_contato;
    }

    public void setNome_contato(String nome_contato) {
        this.nome_contato = nome_contato;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getInscricao() {
        return inscricao;
    }

    public void setInscricao(String inscricao) {
        this.inscricao = inscricao;
    }

    public Date getDat_ultima_compra() {
        return dat_ultima_compra;
    }

    public void setDat_ultima_compra(Date dat_ultima_compra) {
        this.dat_ultima_compra = dat_ultima_compra;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.id;
        hash = 53 * hash + Objects.hashCode(this.cnpj);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Fornecedores other = (Fornecedores) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.cnpj, other.cnpj)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        return sb.append(id).append(";")
                .append(razao_social).append(";")
                .append(nome_fantasia).append(";")
                .append(telefone).append(";")
                .append(celular).append(";")
                .append(nome_contato).append(";")
                .append(endereco).append(";")
                .append(numero).append(";")
                .append(bairro).append(";")
                .append(cidade).append(";")
                .append(estado).append(";")
                .append(cnpj).append(";")
                .append(inscricao).append(";")
                .append(dat_ultima_compra).append(";").toString();
    }

}
