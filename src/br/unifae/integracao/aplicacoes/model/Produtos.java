package br.unifae.integracao.aplicacoes.model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Carlos Francisco
 */
public class Produtos implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private String nome;
    private String codigoBarras;
    private Date dat_ultima_compra;
    private Date dat_ultima_venda;
    private double vlr_custo;
    private double vlr_venda;
    private double margem_lucro;
    private int estoque;
    private int fornecedores_id;

    public Produtos() {
    }

    public Produtos(int id) {
        this.id = id;
    }

    public Produtos(
            int id,
            String nome,
            String codigo_barras,
            Date dat_ultima_compra,
            Date dat_ultima_venda,
            double vlr_custo,
            double vlr_venda,
            double margem_lucro,
            int estoque,
            int fornecedores_id
    ) {
        this.id = id;
        this.nome = nome;
        this.codigoBarras = codigo_barras;
        this.dat_ultima_compra = dat_ultima_compra;
        this.dat_ultima_venda = dat_ultima_venda;
        this.vlr_custo = vlr_custo;
        this.vlr_venda = vlr_venda;
        this.margem_lucro = margem_lucro;
        this.estoque = estoque;
        this.fornecedores_id = fornecedores_id;
    }

    public Produtos(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public Date getDat_ultima_compra() {
        return dat_ultima_compra;
    }

    public void setDat_ultima_compra(Date dat_ultima_compra) {
        this.dat_ultima_compra = dat_ultima_compra;
    }

    public Date getDat_ultima_venda() {
        return dat_ultima_venda;
    }

    public void setDat_ultima_venda(Date dat_ultima_venda) {
        this.dat_ultima_venda = dat_ultima_venda;
    }

    public double getVlr_custo() {
        return vlr_custo;
    }

    public void setVlr_custo(double vlr_custo) {
        this.vlr_custo = vlr_custo;
    }

    public double getVlr_venda() {
        return vlr_venda;
    }

    public void setVlr_venda(double vlr_venda) {
        this.vlr_venda = vlr_venda;
    }

    public double getMargem_lucro() {
        return margem_lucro;
    }

    public void setMargem_lucro(double margem_lucro) {
        this.margem_lucro = margem_lucro;
    }

    public int getEstoque() {
        return estoque;
    }

    public void setEstoque(int estoque) {
        this.estoque = estoque;
    }

    public int getFornecedores_id() {
        return fornecedores_id;
    }

    public void setFornecedores_id(int fornecedores_id) {
        this.fornecedores_id = fornecedores_id;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Produtos other = (Produtos) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        return sb.append(id).append(";")
                .append(nome).append(";")
                .append(codigoBarras).append(";")
                .append(dat_ultima_compra).append(";")
                .append(dat_ultima_venda).append(";")
                .append(vlr_custo).append(";")
                .append(vlr_venda).append(";")
                .append(margem_lucro).append(";")
                .append(estoque).append(";")
                .append(fornecedores_id).append(";").toString();
    }
}
