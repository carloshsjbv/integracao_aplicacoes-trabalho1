package br.unifae.integracao.aplicacoes.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Carlos Francisco
 */
public class Clientes implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private String nome;
    private Date dat_nascimento;
    private String cpf;
    private String rg;
    private String telefone;
    private String celular;
    private double limite_credito;
    private String endereco;
    private String numero;
    private String bairro;
    private String cidade;
    private String estado;
    private Date dat_ultima_venda;

    public Clientes() {
    }

    public Clientes(int id) {
        this.id = id;
    }

    public Clientes(
            int id,
            String nome,
            Date dat_nascimento,
            String cpf,
            String rg,
            String telefone,
            String celular,
            double limite_credito,
            String endereco,
            String numero,
            String bairro,
            String cidade,
            String estado,
            Date dat_ultima_venda
    ) {
        this.id = id;
        this.nome = nome;
        this.dat_nascimento = dat_nascimento;
        this.cpf = cpf;
        this.rg = rg;
        this.telefone = telefone;
        this.celular = celular;
        this.limite_credito = limite_credito;
        this.endereco = endereco;
        this.numero = numero;
        this.bairro = bairro;
        this.cidade = cidade;
        this.estado = estado;
        this.dat_ultima_venda = dat_ultima_venda;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDat_nascimento() {
        return dat_nascimento;
    }

    public void setDat_nascimento(Date dat_nascimento) {
        this.dat_nascimento = dat_nascimento;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public double getLimite_credito() {
        return limite_credito;
    }

    public void setLimite_credito(double limite_credito) {
        this.limite_credito = limite_credito;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getDat_ultima_venda() {
        return dat_ultima_venda;
    }

    public void setDat_ultima_venda(Date dat_ultima_venda) {
        this.dat_ultima_venda = dat_ultima_venda;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + this.id;
        hash = 31 * hash + Objects.hashCode(this.cpf);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Clientes other = (Clientes) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.cpf, other.cpf)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        return sb.append(id).append(";")
                .append(nome).append(";")
                .append(dat_nascimento).append(";")
                .append(cpf).append(";")
                .append(rg).append(";")
                .append(telefone).append(";")
                .append(celular).append(";")
                .append(limite_credito).append(";")
                .append(endereco).append(";")
                .append(numero).append(";")
                .append(bairro).append(";")
                .append(cidade).append(";")
                .append(estado).append(";")
                .append(dat_ultima_venda).toString();
    }
}
