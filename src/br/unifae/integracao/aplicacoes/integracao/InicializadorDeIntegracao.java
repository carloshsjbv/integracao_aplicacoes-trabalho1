package br.unifae.integracao.aplicacoes.integracao;

import br.unifae.integracao.aplicacoes.controllers.ClientesController;
import br.unifae.integracao.aplicacoes.controllers.FornecedoresController;
import br.unifae.integracao.aplicacoes.controllers.ProdutosController;
import br.unifae.integracao.aplicacoes.exceptions.DBErrorException;
import br.unifae.integracao.aplicacoes.model.Clientes;
import br.unifae.integracao.aplicacoes.model.Fornecedores;
import br.unifae.integracao.aplicacoes.model.Produtos;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.Locale;

/**
 * Classe responsável por gravar os arquivos da aplicação de compras no banco de
 * dados da aplicação de cadastros.
 *
 * @author Carlos H
 */
public class InicializadorDeIntegracao {

    private final FornecedoresController fornecedoresController;
    private final ProdutosController produtosController;
    private final ClientesController clientesController;

    public InicializadorDeIntegracao() {
        this.clientesController = new ClientesController();
        this.produtosController = new ProdutosController();
        this.fornecedoresController = new FornecedoresController();
    }

    protected void buildObject(String path, String linha, String operacao) throws IOException, ClassNotFoundException, ParseException, DBErrorException {

        String[] objetoParticionado = linha.split(";");

        if (path.contains("clientes.csv")) {
            Clientes cliente = new Clientes(
                    Integer.parseInt(objetoParticionado[1]),
                    objetoParticionado[2],
                    converteDataDoCSV(objetoParticionado[3]),
                    objetoParticionado[4],
                    objetoParticionado[5],
                    objetoParticionado[6],
                    objetoParticionado[7],
                    Double.parseDouble(objetoParticionado[8]),
                    objetoParticionado[9],
                    objetoParticionado[10],
                    objetoParticionado[11],
                    objetoParticionado[12],
                    objetoParticionado[13],
                    Date.from(Instant.now())
            );

            switch (operacao) {
                case "0":
                    cliente.setId(0);
                    clientesController.salvar(cliente);
                    break;
                case "1":
                    clientesController.update(cliente);
                    break;
                case "2":
                    clientesController.delete(cliente.getId());
                    break;
            }
        } else if (path.contains("fornecedores.csv")) {
            Fornecedores fornecedor = new Fornecedores(
                    Integer.parseInt(objetoParticionado[1]),
                    objetoParticionado[2],
                    objetoParticionado[3],
                    objetoParticionado[4],
                    objetoParticionado[5],
                    objetoParticionado[6],
                    objetoParticionado[7],
                    objetoParticionado[8],
                    objetoParticionado[9],
                    objetoParticionado[10],
                    objetoParticionado[11],
                    objetoParticionado[12],
                    objetoParticionado[13],
                    Date.from(Instant.now())
            );

            switch (operacao) {
                case "0":
                    fornecedor.setId(0);
                    fornecedoresController.salvar(fornecedor);
                    break;
                case "1":
                    fornecedoresController.update(fornecedor);
                    break;
                case "2":
                    fornecedoresController.delete(fornecedor.getId());
                    break;
            }
        } else if (path.contains("produto.csv")) {
            Produtos produto = new Produtos(
                    Integer.parseInt(objetoParticionado[1]),
                    objetoParticionado[2],
                    objetoParticionado[3],
                    Date.from(Instant.now()),
                    null,
                    Double.parseDouble(objetoParticionado[6]),
                    Double.parseDouble(objetoParticionado[7]),
                    Double.parseDouble(objetoParticionado[8]),
                    Integer.parseInt(objetoParticionado[9]), // Aqui deverá ser feito uma busca no banco para puxar o estoque atual e somar este valor.
                    Integer.parseInt(objetoParticionado[10])
            );

            switch (operacao) {
                case "0":
                    produto.setId(0);
                    produtosController.salvar(produto);
                    break;
                case "1":
                    produtosController.update(produto);
                    break;
                case "2":
                    produtosController.delete(produto.getId());
                    break;
            }
        }
    }

    public Date converteDataDoCSV(String dataDoCSV) throws ParseException {
        DateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZ yyyy", Locale.US);
        Date parsedDate = (Date) df.parse(dataDoCSV);

        return parsedDate;
    }

}
