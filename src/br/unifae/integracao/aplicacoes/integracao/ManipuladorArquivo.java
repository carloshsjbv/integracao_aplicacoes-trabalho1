package br.unifae.integracao.aplicacoes.integracao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 *
 * @author 21452-8
 */
public class ManipuladorArquivo {

    private final InicializadorDeIntegracao inicializadorDeIntegracao;

    public ManipuladorArquivo() {
        this.inicializadorDeIntegracao = new InicializadorDeIntegracao();
    }

    public void leitor(String path) throws IOException, Exception {

        try (BufferedReader bufferedRead = new BufferedReader(new FileReader(path))) {
            String linha = "";

            while (true) {

                linha = bufferedRead.readLine();

                if (linha == null || linha.isEmpty()) {
                    break;
                }

                linha = new String(linha.getBytes(), Charset.forName("UTF-8"));
                String operacao = linha.substring(0, 1);

                inicializadorDeIntegracao.buildObject(path, linha, operacao);

            }

            bufferedRead.close();
        }
    }

    public void escritor(String flag, String path, Object obj) throws Exception {

        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path, true));
            bufferedWriter.append(flag + ";" + obj.toString());
            bufferedWriter.newLine();
            bufferedWriter.close();
        } catch (IOException e) {
            throw new Exception();
        }
    }

}
