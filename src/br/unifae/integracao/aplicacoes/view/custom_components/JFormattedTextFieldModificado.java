/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unifae.integracao.aplicacoes.view.custom_components;

import java.awt.event.KeyEvent;
import javax.swing.JFormattedTextField;

/**
 *
 * @author pr_andrade
 */
public final class JFormattedTextFieldModificado extends JFormattedTextField {

    private int maximoCaracteres = -1;// definição de -1 como  valor normal de um textfield sem limite de caracters

    private JFormattedTextFieldModificado() {
        super();
        addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jFormattedTextFieldKeyTyped(evt);
            }
        });
    }
    
    /**
     * JFormattedTextField modificado para limitar quantidade de caracteres
     * 
     * @param maximo tamanho máximo de caracteres permitidos
     */
    public JFormattedTextFieldModificado(int maximo) {
        super();
        setMaximoCaracteres(maximo);// define o tamanho máximo que deve ser aceito no jtextfield que foi recebido no  construtor

        addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jFormattedTextFieldKeyTyped(evt);
            }
        });
    }

    private void jFormattedTextFieldKeyTyped(KeyEvent evt) {
        
        //if para saber se precisa verificar também o tamanho da string do campo maior ou igual ao tamanho máximo, cancela e nao deixa inserir mais
        if ((getText().replace(",", "").length() >= getMaximoCaracteres()) && (getMaximoCaracteres() != -1)) {
            evt.consume();
            // remover os caracters inválidos caso o usuário tenha copiado o conteúdo de algum lugar, ou seja, com tamanho maior que o definido
            setText(getText().substring(0, getMaximoCaracteres()));
            
        }//fim do if do tamanho da string do campo

    }
    
    public int getMaximoCaracteres() {
        return maximoCaracteres;
    }

    public void setMaximoCaracteres(int maximoCaracteres) {
        this.maximoCaracteres = maximoCaracteres;
    }
}
