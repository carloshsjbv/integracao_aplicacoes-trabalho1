/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unifae.integracao.aplicacoes.view.custom_components;

import java.awt.event.KeyEvent;
import javax.swing.JTextField;

/**
 *
 * @author pr_andrade
 */
public final class JTextFieldModificado extends JTextField {

    private int maximoCaracteres = -1;// definição de -1 como  valor normal de um textfield sem limite de caracters
    private int tipo = -1;//definição padrão para aceitar qualquer tipo de caracters

    private JTextFieldModificado() {
        super();
        addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldKeyTyped(evt);
            }
        });
    }
    
    /**
     * JTextField modificado para limitar quantidade de caracteres
     * 
     * @param maximo tamanho máximo de caracteres permitidos
     */
    public JTextFieldModificado(int maximo) {
        super();
        setMaximoCaracteres(maximo);// define o tamanho máximo que deve ser aceito no jtextfield que foi recebido no  construtor

        addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldKeyTyped(evt);
            }
        });
    }

    /**
     * JTextField modificado para aceitar somente caracteres não numéricos ou somente caracteres numéricos com limite de caracteres
     * 
     * @param tipo indica o tipo de caracters aceitos, sendo 0 para caracteres não númericos e 1 para caracteres numéricos
     * @param maximo indica o tamanho máximo de caracteres que serão aceitos no jtextfield.
     */
    public JTextFieldModificado(int tipo, int maximo) {
        super();
        setTipo(tipo);//define o tipo de caracters serão aceitos
        setMaximoCaracteres(maximo);// define o tamanho máximo que deve ser aceito no jtextfield que foi recebido no  construtor

        addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldKeyTyped(evt);
            }
        });
    }

    private void jTextFieldKeyTyped(KeyEvent evt) {

        String caracteres = "0987654321";// lista de caracters que podem ser aceitos
        
        //aceitar caracteres não numéricos
        if (caracteres.contains(evt.getKeyChar() + "") && tipo == 0) {// se o caracter que gerou o evento estiver na lista
            evt.consume();//aciona esse propriedade para eliminar a ação do evento
        }        
        //aceitar apenas caracteres numéricos
        else if (!caracteres.contains(evt.getKeyChar() + "") && tipo == 1) {// se o caracter que gerou o evento não estiver na lista
            evt.consume();//aciona esse propriedade para eliminar a ação do evento
        }
        
        //if para saber se precisa verificar também o tamanho da string do campo maior ou igual ao tamanho máximo, cancela e nao deixa inserir mais
        if ((getText().length() >= getMaximoCaracteres()) && (getMaximoCaracteres() != -1)) {
            evt.consume();
            // remover os caracters inválidos caso o usuário tenha copiado o conteúdo de algum lugar, ou seja, com tamanho maior que o definido
            setText(getText().substring(0, getMaximoCaracteres()));
            
        }//fim do if do tamanho da string do campo

    }
    
    public int getTipo() {
        return tipo;
    }

    public int getMaximoCaracteres() {
        return maximoCaracteres;
    }
    
    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public void setMaximoCaracteres(int maximoCaracteres) {
        this.maximoCaracteres = maximoCaracteres;
    }
}
