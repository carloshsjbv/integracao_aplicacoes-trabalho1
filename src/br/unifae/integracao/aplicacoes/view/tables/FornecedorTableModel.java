package br.unifae.integracao.aplicacoes.view.tables;

import br.unifae.integracao.aplicacoes.model.Fornecedores;
import br.unifae.integracao.aplicacoes.model.Produtos;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * Classe de modelo de dados da tabela.
 *
 * @author Carlos H
 */
public class FornecedorTableModel extends AbstractTableModel {

    List<Fornecedores> fornecedores = new ArrayList<>();
    String[] colunas = {"Código", "Nome fantasia", "CNPJ", "Telefone", "Contato", "última Compra"};

    @Override
    public String getColumnName(int column) {
        return colunas[column]; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getRowCount() {
        return fornecedores.size();
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        switch (columnIndex) {
            case 0:
                return fornecedores.get(rowIndex).getId();
            case 1:
                return fornecedores.get(rowIndex).getNome_fantasia();
            case 2:
                return fornecedores.get(rowIndex).getCnpj();
            case 3:
                return fornecedores.get(rowIndex).getTelefone();
            case 4:
                return fornecedores.get(rowIndex).getNome_contato();
            case 5:
                return fornecedores.get(rowIndex).getDat_ultima_compra();

        }

        return null;

    }

    public void adicionaLinha(List<Fornecedores> fornecedores) {

        if (!this.fornecedores.isEmpty()) {
            this.fornecedores.removeAll(this.fornecedores);
        }

        for (Fornecedores fornecedor : fornecedores) {
            this.fornecedores.add(fornecedor);
            this.fireTableDataChanged();
        }

    }

    public void removeLinha(int linha) {
        this.fornecedores.remove(linha);
        this.fireTableRowsDeleted(linha, linha);
    }

    public int getIdLinha(int index) {
        return fornecedores.get(index).getId();
    }

}
