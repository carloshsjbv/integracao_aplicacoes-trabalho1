package br.unifae.integracao.aplicacoes.view.tables;

import br.unifae.integracao.aplicacoes.model.Clientes;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * Classe de modelo de dados da tabela.
 *
 * @author Carlos H
 */
public class ClienteTableModel extends AbstractTableModel {

    List<Clientes> clientes = new ArrayList<>();
    String[] colunas = {"Código", "Nome", "CPF", "RG", "Limite Crédito", "Útima Compra"};

    @Override
    public String getColumnName(int column) {
        return colunas[column]; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getRowCount() {
        return clientes.size();
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        switch (columnIndex) {
            case 0:
                return clientes.get(rowIndex).getId();
            case 1:
                return clientes.get(rowIndex).getNome();
            case 2:
                return clientes.get(rowIndex).getCpf();
            case 3:
                return clientes.get(rowIndex).getRg();
            case 4:
                return clientes.get(rowIndex).getLimite_credito();
            case 5:
                return clientes.get(rowIndex).getDat_ultima_venda();

        }

        return null;

    }

    public void adicionaLinha(List<Clientes> clientes) {

        if (!this.clientes.isEmpty()) {
            this.clientes.removeAll(this.clientes);
        }

        for (Clientes produto : clientes) {
            this.clientes.add(produto);
            this.fireTableDataChanged();
        }

    }

    public void removeLinha(int linha) {
        this.clientes.remove(linha);
        this.fireTableRowsDeleted(linha, linha);
    }

    public int getIdLinha(int index) {
        return clientes.get(index).getId();
    }

}
