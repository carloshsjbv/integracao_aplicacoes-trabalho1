package br.unifae.integracao.aplicacoes.view.tables;

import br.unifae.integracao.aplicacoes.model.Produtos;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * Classe de modelo de dados da tabela.
 *
 * @author Carlos H
 */
public class ProdutoTableModel extends AbstractTableModel {

    List<Produtos> produtos = new ArrayList<>();
    String[] colunas = {"Código", "Nome", "Codigo de Barras", "Valor Custo", "Valor venda", "Estoque"};

    @Override
    public String getColumnName(int column) {
        return colunas[column]; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getRowCount() {
        return produtos.size();
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        switch (columnIndex) {
            case 0:
                return produtos.get(rowIndex).getId();
            case 1:
                return produtos.get(rowIndex).getNome();
            case 2:
                return produtos.get(rowIndex).getCodigoBarras();
            case 3:
                return produtos.get(rowIndex).getVlr_custo();
            case 4:
                return produtos.get(rowIndex).getVlr_venda();
            case 5:
                return produtos.get(rowIndex).getEstoque();

        }

        return null;

    }

    public void adicionaLinha(List<Produtos> produtos) {

        if (!this.produtos.isEmpty()) {
            this.produtos.removeAll(this.produtos);
        }

        for (Produtos produto : produtos) {
            this.produtos.add(produto);
            this.fireTableDataChanged();
        }

    }

    public void removeLinha(int linha) {
        this.produtos.remove(linha);
        this.fireTableRowsDeleted(linha, linha);
    }

    public int getIdLinha(int index) {
        return produtos.get(index).getId();
    }

}
