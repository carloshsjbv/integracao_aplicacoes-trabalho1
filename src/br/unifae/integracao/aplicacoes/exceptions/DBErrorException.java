package br.unifae.integracao.aplicacoes.exceptions;

/**
 *
 * @author Carlos Francisco
 */
public class DBErrorException extends Exception{

    public DBErrorException(String msg) {
        super(msg);
    }
    
}